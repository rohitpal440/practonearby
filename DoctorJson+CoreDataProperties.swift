//
//  DoctorJson+CoreDataProperties.swift
//  
//
//  Created by Rohit Pal on 26/08/16.
//
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension DoctorJson {

    @NSManaged var doctorId: NSNumber?
    @NSManaged var jsonData: String?

}
