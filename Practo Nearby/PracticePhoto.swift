//
//  PracticePhoto.swift
//  Practo Nearby
//
//  Created by Rohit Pal on 22/08/16.
//  Copyright © 2016 Rohit Pal. All rights reserved.
//

import Foundation

class PracticePhoto {
    
    var url: String?
    var isLogo: Bool?
    
    init(json: AnyObject) {
        self.url =  json["url"] as? String
        self.isLogo = json["logo"] as? Bool
    }
    
}