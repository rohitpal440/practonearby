//
//  GenericTextListController.swift
//  Practo Nearby
//
//  Created by Rohit Pal on 25/08/16.
//  Copyright © 2016 Rohit Pal. All rights reserved.
//

import UIKit

class GenericTextListController: UITableViewController {
    var stringArray:[String]?
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the 
        // navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(tableView: UITableView,
                            numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return (stringArray?.count)!
    }

    
    override func tableView(tableView: UITableView,
                            cellForRowAtIndexPath indexPath: NSIndexPath)
        -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier(
            "genericTextListCell", forIndexPath: indexPath)

        cell.textLabel?.text = stringArray![indexPath.row]
        return cell
    }
}
