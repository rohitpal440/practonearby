//
//  Speciality.swift
//  Practo Nearby
//
//  Created by Rohit Pal on 22/08/16.
//  Copyright © 2016 Rohit Pal. All rights reserved.
//

import Foundation

class Speciality {
    let speciality: String?
    let subSpeciality: String?
    let approved: Bool?
    
    init(json: AnyObject) {
        self.speciality = json["specialty"] as? String
        self.subSpeciality = json["sub_specialty"] as? String
        self.approved = json["approved"] as? Bool
    }
    
}