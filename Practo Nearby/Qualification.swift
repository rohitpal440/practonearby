//
//  Qualification.swift
//  Practo Nearby
//
//  Created by Rohit Pal on 22/08/16.
//  Copyright © 2016 Rohit Pal. All rights reserved.
//

import Foundation

class Qualification {
    var qualification: String?
    var isQualficationValidInCountry: Bool?
    var college: String?
    var completionYear: String?
    init(json: AnyObject) {
        self.qualification = json["qualification"] as? String
        self.isQualficationValidInCountry =
            json["is_qualification_valid_in_country"] as? Bool
        self.college = json["college"] as? String
        self.completionYear =  json["completion_year"] as? String
    }
}
