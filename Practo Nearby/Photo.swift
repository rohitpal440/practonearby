//
//  Photo.swift
//  Practo Nearby
//
//  Created by Rohit Pal on 22/08/16.
//  Copyright © 2016 Rohit Pal. All rights reserved.
//

import Foundation

class Photo {
    let url: String?
    let isDefault: Bool?
    init(json: AnyObject) {
        self.url =  json["url"] as? String
        self.isDefault = json["is_default"] as? Bool
    }
}