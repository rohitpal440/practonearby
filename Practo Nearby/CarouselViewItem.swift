//
//  CarouselViewItem.swift
//  Practo Nearby
//
//  Created by Rohit Pal on 23/08/16.
//  Copyright © 2016 Rohit Pal. All rights reserved.
//

import UIKit

class CarouselViewItem: UIView {

    @IBOutlet weak var doctorPhoto: UIImageView!
    @IBOutlet weak var doctorName: UILabel!
//    @IBOutlet weak var doctorQualification: UILabel!
    @IBOutlet weak var doctorSpeciality: UILabel!
    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */

}
