//
//  DoctorDetailHeaderCell.swift
//  Practo Nearby
//
//  Created by Rohit Pal on 24/08/16.
//  Copyright © 2016 Rohit Pal. All rights reserved.
//

import UIKit

class DoctorDetailHeaderCell: UITableViewCell {
    
    @IBOutlet weak var backgroundPracticeImage: UIImageView!
    @IBOutlet weak var doctorProfileImage: UIImageView!
    @IBOutlet weak var doctorName: UILabel!
    @IBOutlet weak var doctorQualifications: UILabel!
    
    @IBOutlet weak var doctorSpecialities: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func setupCell(doctor:Doctor){
        self.doctorName.text = doctor.name
        //concatenate all qualifications
        var qualifications:String = ""
        for qual in doctor.qualifications!{
            qualifications += "\(qual.qualification!), "
        }
        self.doctorQualifications.text = qualifications
        self.doctorSpecialities.text = doctor.specialities![0].speciality
        // set profile image
        var imageUrl:String?
        if doctor.photos?.count > 0 {
            imageUrl = doctor.defaultDoctorPhoto!.url
        } else {
            // Load a placeholder Image
            imageUrl = "https://www.placehold.it/96x96?text=NA"
        }
        
        let placeholderImage = UIImage(named: "CarouselPlaceholder")
        self.doctorProfileImage.kf_setImageWithURL(NSURL(string: imageUrl!)!,
                                            placeholderImage: placeholderImage)
        self.doctorProfileImage.contentMode = .ScaleAspectFit
        self.doctorProfileImage.layer.borderWidth = 1
        self.doctorProfileImage.layer.borderColor = UIColor.whiteColor().CGColor
        let backgroundImageUrl:String?
        // Set Background Image
        if doctor.practicePhotos?.count > 0 {
            
            backgroundImageUrl = doctor.practicePhotos![0].url
        } else {
            // Load a placeholder Image
            backgroundImageUrl = "https://www.placehold.it/320x320?text=NA"
        }

        self.backgroundPracticeImage.kf_setImageWithURL(
            NSURL(string: backgroundImageUrl!)!,
            placeholderImage: placeholderImage)
        backgroundPracticeImage.tintColor = UIColor(white: 0.0, alpha: 0.7)
    }
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
