//
//  ConsultationFeesCell.swift
//  Practo Nearby
//
//  Created by Rohit Pal on 24/08/16.
//  Copyright © 2016 Rohit Pal. All rights reserved.
//

import UIKit
import CoreData
class ConsultationFeesCell: UITableViewCell {

    @IBOutlet weak var favouriteButton: UIButton!
    @IBOutlet weak var consultationFees: UILabel!
    var isFavourite = false
    var doctor:Doctor?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    @IBAction func addToFavourite(sender: AnyObject) {
        if !isFavourite{
            saveDoctorToDatabase()
            favouriteButton.highlighted = true
            favouriteButton.selected = true
            isFavourite = true
        } else {
            removeDoctorFromDatabase()
            favouriteButton.highlighted = false
            favouriteButton.selected = false
            isFavourite = false
        }
    }
    
    func saveDoctorToDatabase() {
//      create an instance of our managedObjectContext
        let managedContext = DataController().managedObjectContext
        let doctorJson = NSFetchRequest(entityName: "DoctorJson")
        let predicate = NSPredicate(format: "doctorId == %d",
                                    (doctor!.doctorId)!)
        do {
            doctorJson.predicate = predicate
            if let doctorsJson = try managedContext
                .executeFetchRequest(doctorJson) as? [DoctorJson]{

                if doctorsJson.count == 0 {
                    // we set up our entity by selecting the entity and context 
                    // that we're targeting
                    let entity =
                        NSEntityDescription.insertNewObjectForEntityForName(
                            "DoctorJson", inManagedObjectContext: managedContext
                            ) as! DoctorJson
                    
                    // add our data
                    if (doctor?.toNSString()) != nil{
                        entity.setValue(doctor!.toNSString()!,
                                        forKey: "jsonData")
                        entity.setValue((doctor!.doctorId)!,
                                        forKey: "doctorId")
                        // we save our entity
                        do {
                            try managedContext.save()
                            NSUserDefaults.standardUserDefaults()
                                .setObject(true,
                                           forKey: "\((doctor!.doctorId)!)")
                            NSUserDefaults.standardUserDefaults().synchronize()
                        } catch {
                            fatalError("Failure to save context: \(error)")
                        }
                    }
                } else{
                    print("Doctor Already Exist")
                }
                
            }
        } catch {
            fatalError("Failed to fetch Doctor: \(error)")
        }
    }
    
    func removeDoctorFromDatabase() {
//        delete object with doctor id
//        create an instance of our managedObjectContext
        let managedContext = DataController().managedObjectContext
        let doctorJson = NSFetchRequest(entityName: "DoctorJson")
        let predicate = NSPredicate(format: "doctorId == %d",
                                    (doctor!.doctorId)!)
        do {
            doctorJson.predicate = predicate
            if let doctorsJson =
                try managedContext.executeFetchRequest(doctorJson)
                    as? [DoctorJson]{
                if(doctorsJson.count == 1){
                    let d = doctorsJson.first
                    do{
                        managedContext.deleteObject(d!)
                        try managedContext.save()
                    } catch {
                        print("Unable to delete object")
                    }
                    NSUserDefaults.standardUserDefaults().removeObjectForKey(
                        "\((doctor!.doctorId)!)")
                    NSUserDefaults.standardUserDefaults().synchronize()
                }
                do{
                    try managedContext.save()
                }
                catch{
                    
                }
            } else {
                print("No doctor to delete with this id")
            }
        } catch {
            fatalError("Failed to fetch Doctor: \(error)")
        }
    }
    
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func setupCell(doctor:Doctor) {
        self.doctor = doctor
        favouriteButton.setImage(UIImage(named: "HeartDefault"),
                                 forState: .Normal)
        favouriteButton.setImage(UIImage(named: "HeartRed"),
                                 forState: .Selected)
        favouriteButton.setImage(UIImage(named: "HeartRed"),
                                 forState: .Highlighted)
        self.consultationFees.text =
            "₹ \(doctor.consultationFees!) Consultation Fees"
        if NSUserDefaults.standardUserDefaults()
            .objectForKey("\((doctor.doctorId)!)") != nil{
            favouriteButton.highlighted = true
            isFavourite = true
        }
    }
}
