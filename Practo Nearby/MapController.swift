//
//  MapController.swift
//  Practo Nearby
//
//  Created by Rohit Pal on 18/08/16.
//  Copyright © 2016 Rohit Pal. All rights reserved.
//

import UIKit
import GoogleMaps
import Kingfisher

class MapController: UIViewController {

    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var mapView: GMSMapView!
    var currentLocation:CLLocation?
    var doctorsList:[Doctor]?
    var selectedDoctorIndex: Int = 1
    
    @IBOutlet weak var carousel: iCarousel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        carousel.type = .Linear
        carousel.dataSource = self
        carousel.delegate = self
        mapView.delegate = self
        mapView.myLocationEnabled = true
        mapView.settings.myLocationButton = true
        self.mapView.camera = GMSCameraPosition(
            target: currentLocation!.coordinate, zoom: 14, bearing: 0,
            viewingAngle: 0)
        showMarkersOnMap()
    }
    
    
    func showMarkersOnMap() {
        for i in 0..<doctorsList!.count{
            let doctor = doctorsList![i]
            let position = CLLocationCoordinate2D(latitude: doctor.latitude!,
                                                  longitude: doctor.longitude!)
            let marker = GMSMarker(position: position)
            marker.snippet = doctor.specialities![0].speciality
            marker.title = doctor.name
            marker.userData = i
            marker.icon = UIImage(named: "LocationMarkerDefault")
            marker.appearAnimation = kGMSMarkerAnimationPop
            marker.map = mapView
        }
    }
    
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        carousel.scrollToItemAtIndex(self.selectedDoctorIndex, animated: true)
        self.view.bringSubviewToFront(self.searchBar)
//        self.view.bringSubviewToFront(searchOptionCollectionView)
        
    }
    
    @IBAction func showFavouriteDoctors(sender: AnyObject) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let favouriteDoctorsController =
            storyboard.instantiateViewControllerWithIdentifier(
                "FavouriteDoctors") as! FavouriteDoctors
        navigationController?.pushViewController(
            favouriteDoctorsController, animated: true)
    }
}


extension MapController: GMSMapViewDelegate {
    func mapView(mapView: GMSMapView, didTapMarker marker: GMSMarker) -> Bool {
        let data = marker.userData
        showDoctorDetails(data! as! Int)
        return false
    }
    
    func mapView(mapView: GMSMapView,
                 didTapAtCoordinate coordinate: CLLocationCoordinate2D) {
        if self.searchBar.isFirstResponder(){
            self.searchBar.resignFirstResponder()
        }
    }

    
    func showDoctorDetails(index:Int) {
        self.selectedDoctorIndex = index
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let doctorDetailController =
            storyboard.instantiateViewControllerWithIdentifier(
                "DoctorDetailController") as! DoctorDetailController
        doctorDetailController.doctor = doctorsList![index]
        navigationController?.pushViewController(doctorDetailController,
                                                 animated: true)
    }
    
}

extension MapController: UISearchBarDelegate{
    
    func searchBarTextDidBeginEditing(searchBar: UISearchBar) {
//        searchResultTableView.hidden = false
    }
    
    func mapTapped(sender:UITapGestureRecognizer){
        self.searchBar.resignFirstResponder()
        sender.removeTarget(self, action: nil)
    }
    
    func searchBarSearchButtonClicked(searchBar: UISearchBar) {
        var searchString = searchBar.text
        searchString = searchString!
            .stringByReplacingOccurrencesOfString(" ", withString: "")
        
        if (searchString != nil)  {
            if searchString != "" {
                searchBar.endEditing(true)
                if searchString == "All" || searchString == "all"{
                    searchString = " "
                }
                let latlng = NSUserDefaults.standardUserDefaults()
                    .objectForKey("latlng") as! String
                let cityName = NSUserDefaults.standardUserDefaults()
                    .objectForKey("cityName") as! String
                if NetworkTask.sharedInstance.isConnected(self){
                    NetworkTask.sharedInstance
                        .getNearbyDoctors(cityName,latlng: latlng,
                                speciality:searchString!) { doctors, error in
                                    if doctors?.count > 0 {
                                        self.updateDoctorList(doctors!)
                                    } else{
                                        NetworkTask.sharedInstance
                                            .noResultFoundAlert(self)
                                    }
                    }
                }
            } else {
                let AlertView = UIAlertController(
                    title: "Invalid Speciality!",
                    message: "Search text can't be empty",
                    preferredStyle: UIAlertControllerStyle.Alert)
                AlertView.addAction(UIAlertAction(title: "OK",
                    style: UIAlertActionStyle.Cancel, handler:nil))
                self.presentViewController(AlertView, animated: true,
                                               completion: nil)
                searchBar.endEditing(false)
            }
            
        }
    }
    
    func updateDoctorList(doctors:[Doctor]) {
        self.doctorsList = doctors
        carousel.reloadData()
        mapView.clear()
        showMarkersOnMap()
    }
    
//    func searchBar(searchBar: UISearchBar, textDidChange searchText: String) {
//        print("searching string \(searchText)")
//    }
    
}

// This is the extension for showing the suggestions
//
//extension MapController: UICollectionViewDataSource {
//    func collectionView(collectionView: UICollectionView,
//                        numberOfItemsInSection section: Int) -> Int {
//        return 10
//    }
//    
//    func collectionView(collectionView: UICollectionView,
//                        cellForItemAtIndexPath indexPath: NSIndexPath)
//        -> UICollectionViewCell {
//        let cell = collectionView.dequeueReusableCellWithReuseIdentifier(
//            "searchResultCell", forIndexPath: indexPath) as! SearchOptionCell
////        cell.buttonImage.imageView =
//        return cell
//    }
//}

// Remove Replace iCarousel with Collection View 
extension MapController: iCarouselDataSource, iCarouselDelegate {
    
    func numberOfItemsInCarousel(carousel: iCarousel) -> Int {
        return (doctorsList?.count)!
    }
    
    func carousel(carousel: iCarousel, viewForItemAtIndex index: Int,
                  reusingView view: UIView?) -> UIView {
        var carouselViewItem: CarouselViewItem
        let placeholderImage = UIImage(named: "CarouselPlaceholder")
        let imageUrl:String?
        if doctorsList![index].photos?.count > 0 {
            imageUrl = doctorsList![index].defaultDoctorPhoto!.url
        } else {
            // Load a placeholder Image
            imageUrl = "https://www.placehold.it/64x64?text=NA"
        }
        //create new view if no view is available for recycling
        if (view == nil) {
            carouselViewItem = NSBundle.mainBundle()
                .loadNibNamed("CarouselViewItem", owner: nil,
                              options: nil).first as! CarouselViewItem
            carouselViewItem.frame = CGRect(
                x:0, y:0, width:96,height:CGRectGetHeight(self.carousel.frame)
            )
            carouselViewItem.contentMode = .ScaleAspectFit
        } else {
            //get a reference to the label in the recycled view
            carouselViewItem = view as! CarouselViewItem
        }
        carouselViewItem.doctorName.text = doctorsList![index].name!
        carouselViewItem.doctorSpeciality.text = doctorsList![index]
            .specialities![0].speciality
        
        carouselViewItem.doctorPhoto.kf_setImageWithURL(
            NSURL(string: imageUrl! )!, placeholderImage: placeholderImage)
        
        return carouselViewItem
    }
    
    func carousel(carousel: iCarousel, valueForOption option: iCarouselOption,
                  withDefault value: CGFloat) -> CGFloat {
        if (option == .Spacing)
        {
            return value * 1.0
        }
        return value
    }
    
    func carousel(carousel: iCarousel, didSelectItemAtIndex index: Int) {
        showDoctorDetails(index)
    }
    
}