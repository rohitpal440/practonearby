//
//  DoctorDetailController.swift
//  Practo Nearby
//
//  Created by Rohit Pal on 23/08/16.
//  Copyright © 2016 Rohit Pal. All rights reserved.
//

import UIKit

class DoctorDetailController: UITableViewController {

    var doctor:Doctor?
    
    var cellIdentifiers = [
        "DoctorDetailHeaderCell",
        "ExperienceCell",
        "ConsultationFeesCell",
        "PracticeLocationCell",
        "AvailableTodayCell",
        "ServicesCell"
    ]
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBarHidden = false
        self.navigationController?.view.backgroundColor = UIColor.clearColor()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between 
        // presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the 
        // navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
        
        
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        // Befor destroying the view we are going to display the navigation bar 
        // to other Views
        self.navigationController?.navigationBarHidden = true
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 6
    }

    override func tableView(tableView: UITableView,
                            numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 1
    }

    
    override func tableView(tableView: UITableView,
                            heightForRowAtIndexPath indexPath: NSIndexPath)
        -> CGFloat {
        switch indexPath.section {
            //Todo: create automated calculation of row height
        case 0:
            return 256
        case 1:
            return 32
        case 2:
            return 32
        case 3:
            return 192
        case 4:
            return 76
        case 5:
            let servicesHeight = (30 * (self.doctor?.popularServices?.count)!)
            return CGFloat(35 + servicesHeight)
        default:
            return tableView.rowHeight
        }
        
    }
    
    override func tableView(tableView: UITableView,
                            cellForRowAtIndexPath indexPath: NSIndexPath)
        -> UITableViewCell {
        let superCell = tableView.dequeueReusableCellWithIdentifier(
            cellIdentifiers[indexPath.section], forIndexPath: indexPath)
        if superCell is DoctorDetailHeaderCell {
            let cell = superCell as! DoctorDetailHeaderCell
            cell.setupCell(doctor!)
            return cell
        } else if superCell is ExperienceCell {
            let cell = superCell as! ExperienceCell
            cell.setupCell(doctor!)
            return cell
        } else if superCell is ConsultationFeesCell {
            let cell = superCell as! ConsultationFeesCell
            cell.setupCell(doctor!)
            return cell
        } else if superCell is PracticeLocationCell {
            let cell = superCell as! PracticeLocationCell
            cell.setupCell(doctor!)
            return cell
        } else if superCell is AvailableTodayCell {
            let cell = superCell as! AvailableTodayCell
            cell.setupCell(doctor!, delegate: self)
            return cell
        } else if superCell is ServicesCell {
            let cell = superCell as! ServicesCell
            cell.setupCell(doctor!)
            return cell
        } else {
            return superCell
        }
        
    }
}
