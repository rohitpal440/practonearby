//
//  FavouriteDoctors.swift
//  Practo Nearby
//
//  Created by Rohit Pal on 23/08/16.
//  Copyright © 2016 Rohit Pal. All rights reserved.
//

import UIKit
import CoreData
import Kingfisher

class FavouriteDoctors: UITableViewController {
    var doctorsList:[Doctor] = []
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBarHidden = false
        loadFavouriteDoctorFromDatabase()
        tableView.reloadData()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between 
        // presentations
        // self.clearsSelectionOnViewWillAppear = false
        self.tableView.tableFooterView = UIView(frame: CGRectZero)
    }
    
    // Fetch all doctors form database
    func loadFavouriteDoctorFromDatabase() {
        let moc = DataController().managedObjectContext
        let doctorJson = NSFetchRequest(entityName: "DoctorJson")
        do {
            doctorsList = []
            let fetchedDoctors = try moc.executeFetchRequest(doctorJson)
                as! [DoctorJson]
            for doctorData in fetchedDoctors{
                let jsonData = doctorData.parseJSONString
                let doctor = Doctor(json: jsonData!)
                doctorsList.append(doctor)
            }
        } catch {
            fatalError("Failed to fetch person: \(error)")
        }
    }
    
    // Remove Doctor from Database and favourite list
    @IBAction func removeFromFavourite(sender: UIButton) {
        let managedContext = DataController().managedObjectContext
        let doctorJson = NSFetchRequest(entityName: "DoctorJson")
        let doctor = doctorsList[sender.tag]
        print((doctor.doctorId)!)
                let predicate = NSPredicate(format: "doctorId == %d",
                                            (doctor.doctorId)!)
                do {
                    doctorJson.predicate = predicate
                    if let doctorsJson = try managedContext
                        .executeFetchRequest(doctorJson) as? [DoctorJson]{
                        if(doctorsJson.count == 1){
                            let d = doctorsJson.first
                            do{
                                managedContext.deleteObject(d!)
                                try managedContext.save()
                            } catch {
                                print("Unable to delete object")
                            }
                            NSUserDefaults.standardUserDefaults()
                                .removeObjectForKey("\((doctor.doctorId)!)")
                            NSUserDefaults.standardUserDefaults().synchronize()
                        }
                        do{
                            try managedContext.save()
                        }
                        catch{
        
                        }
                        // reload the tableview
                        doctorsList.removeAtIndex(sender.tag)
                        tableView.reloadData()
                    } else {
                        print("No doctor to delete with this id")
                    }
                } catch {
                    fatalError("Failed to fetch Doctor: \(error)")
                }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        // Befor destroying the view we are going to display the navigation bar
        // to other Views
        self.navigationController?.navigationBarHidden = true
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        if doctorsList.count > 0 {
            tableView.backgroundView = .None
            return 1
        } else {
            TableViewHelper
                .EmptyMessage(
                    "You have not favourited any Doctor.\n" +
                    "You Can Do it in Doctor Detail View.",
                    viewController: self)
            return 0
        }
    }

    override func tableView(tableView: UITableView,
                            numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return doctorsList.count
    }

    
    override func tableView(tableView: UITableView,
                            cellForRowAtIndexPath indexPath: NSIndexPath)
        -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier(
            "FavouriteDoctorCell", forIndexPath: indexPath)
            as? FavouriteDoctorCell
        // Configure the cell...
        let doctor:Doctor = doctorsList[indexPath.row]
        cell?.setupCell(doctor, index: indexPath.row)
        cell!.imageView?.contentMode = .ScaleAspectFit
        return cell!
    }
    
    override func tableView(tableView: UITableView,
                            didSelectRowAtIndexPath indexPath: NSIndexPath) {
        showDoctorDetails(indexPath.row)
    }
    
    // show Doctor Detail screen
    func showDoctorDetails(index:Int) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let doctorDetailController = storyboard
            .instantiateViewControllerWithIdentifier(
            "DoctorDetailController") as! DoctorDetailController
        doctorDetailController.doctor = doctorsList[index]
        navigationController?.pushViewController(doctorDetailController,
                                                 animated: true)
    }
    
}
