//
//  AvailableTodayCell.swift
//  Practo Nearby
//
//  Created by Rohit Pal on 24/08/16.
//  Copyright © 2016 Rohit Pal. All rights reserved.
//

import UIKit

class AvailableTodayCell: UITableViewCell {

    let dayNames = ["sunday","monday","tuesday","wednesday","thursday",
                    "friday","saturday"]
    var allTiming:[String:[String:String]]?
    var allSessionList:[String]?
    @IBOutlet weak var todayAvailability: UILabel!
    @IBOutlet weak var todayTiming: UILabel!
    var doctor:Doctor?
    var tableView: UITableViewController?
    @IBAction func showAllTimings(sender: AnyObject) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let genericTextListController = storyboard
            .instantiateViewControllerWithIdentifier(
                "GenericTextListController")
            as! GenericTextListController

        genericTextListController.stringArray = allSessionList
        self.tableView!.presentViewController(genericTextListController,
                                              animated: true, completion: nil)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setupCell(doctor:Doctor, delegate:UITableViewController){
        self.tableView = delegate
        self.doctor = doctor
        let workingDays = doctor.availability!
        self.allTiming = doctor.visitTiming!
        let day = getDayOfWeek()
        if (workingDays[day] == true) {
            todayAvailability.text = "Available Today"
            todayTiming.text = sessionOfDay(allTiming![day]!)
        } else {
            todayAvailability.text = "Closed Today"
            todayAvailability.textColor = UIColor.redColor()
            todayTiming.text = "Doctor is not Available Today"
        }
        allSessionList = allTimingList(allTiming!, workingDays: workingDays)
    }

    func getDayOfWeek()->String {
        let todayDate = NSDate()
        // MARK: remove NSGregorianCalendare as it's depreicated
        let myCalendar = NSCalendar(calendarIdentifier: NSCalendarIdentifierGregorian)
        let myComponents = myCalendar?.components(.Weekday, fromDate: todayDate)
        return dayNames[(myComponents?.weekday)! - 1]
    }
    
    func sessionOfDay(todayTiming: [String:String]) -> String {
        var session:String? = " "
        if todayTiming["sesession1_start_time"] != "" {
            let firstSession:String =
                "\(todayTiming["sesession1_start_time"]!) Hrs -" +
                    " \(todayTiming["sesession1_end_time"]!) Hrs"
            session?.appendContentsOf(firstSession)
        }
        if todayTiming["sesession2_start_time"] != ""{
            let secondSession:String =
                " | \(todayTiming["sesession2_start_time"]!) Hrs -" +
                    " \(todayTiming["sesession2_end_time"]!) Hrs"
            session?.appendContentsOf(secondSession)
        }
        return session!
    }
    
    func allTimingList(allTiming:[String:[String:String]],
                       workingDays:[String:Bool]) -> [String] {
        var list:[String] = []
        for day in dayNames {
            if (workingDays[day] != false) {
                var session = day.uppercaseString
                session += sessionOfDay(allTiming[day]!)
                list.append(session)
            }
        }
        return list
    }
    
}
