//
//  ServicesCell.swift
//  Practo Nearby
//
//  Created by Rohit Pal on 24/08/16.
//  Copyright © 2016 Rohit Pal. All rights reserved.
//

import UIKit

class ServicesCell: UITableViewCell {

    @IBOutlet weak var popularServices: UITableView!
    var serviceArray : [String]? = nil
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

   
    
    func setupCell(doctor:Doctor) {
        
        self.serviceArray = doctor.popularServices
    }
}

extension ServicesCell: UITableViewDataSource{
    
    func tableView(tableView: UITableView,
                   numberOfRowsInSection section: Int) -> Int {
        return self.serviceArray?.count ?? 0
    }
    
    func tableView(tableView: UITableView,
                   heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 30.0
    }
    
    func tableView(tableView: UITableView,
                   cellForRowAtIndexPath indexPath: NSIndexPath)
        -> UITableViewCell {
        let cell = tableView
            .dequeueReusableCellWithIdentifier("reuseCell",
                                               forIndexPath: indexPath)
        cell.textLabel?.text = "- \(serviceArray![indexPath.row])"
        cell.textLabel!.font = UIFont.systemFontOfSize(14.0)
        return cell
    }
    
}

extension ServicesCell: UITableViewDelegate{
    
    func tableView(tableView: UITableView,
                   didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
    }
    
}