//
//  DoctorJson.swift
//  
//
//  Created by Rohit Pal on 26/08/16.
//
//

import Foundation
import CoreData


class DoctorJson: NSManagedObject {

// Insert code here to add functionality to your managed object subclass

    var parseJSONString: AnyObject? {
        if let savedJsonString = self.jsonData {
            
            let data = savedJsonString.dataUsingEncoding(NSUTF8StringEncoding,
                                                    allowLossyConversion: false)
            do{
                if let jsonData = data {
                    // Will return an object or nil if JSON decoding fails
                    return try NSJSONSerialization.JSONObjectWithData(jsonData,
                                options: NSJSONReadingOptions.MutableContainers)
                } else {
                    // Lossless conversion of the string was not possible
                    return nil
                }
                
            } catch {
                print("Error in Converting jsonData to json")
            }
            
        }
        return nil
    }
    
}
