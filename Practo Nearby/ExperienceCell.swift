//
//  ExperienceCell.swift
//  Practo Nearby
//
//  Created by Rohit Pal on 24/08/16.
//  Copyright © 2016 Rohit Pal. All rights reserved.
//

import UIKit

class ExperienceCell: UITableViewCell {

    @IBOutlet weak var recommendation: UILabel!
    @IBOutlet weak var experience: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setupCell(doctor:Doctor) {
        self.recommendation.text =
           "\(doctor.recommendationPercent!)% (\(doctor.recommendation!) votes)"
        self.experience.text = "\(doctor.experienceYears!) years of experience"
    }

}
