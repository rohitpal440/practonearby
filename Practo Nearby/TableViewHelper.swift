//
//  TableViewHelper.swift
//  Practo Nearby
//
//  Created by Rohit Pal on 26/08/16.
//  Copyright © 2016 Rohit Pal. All rights reserved.
//

import Foundation
import UIKit

class TableViewHelper {
    
    class func EmptyMessage(message:String,
                            viewController:UITableViewController) {
        let messageLabel = UILabel(frame: CGRectMake(0,0,
            viewController.view.bounds.size.width,
            viewController.view.bounds.size.height))
        messageLabel.text = message
        messageLabel.textColor = UIColor.blackColor()
        messageLabel.numberOfLines = 0;
        messageLabel.textAlignment = .Center;
        messageLabel.font = UIFont(name: "TrebuchetMS", size: 15)
        messageLabel.sizeToFit()
        
        viewController.tableView.backgroundView = messageLabel;
        viewController.tableView.separatorStyle = .None;
    }
}