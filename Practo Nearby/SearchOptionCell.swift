//
//  SearchOptionCell.swift
//  Practo Nearby
//
//  Created by Rohit Pal on 27/08/16.
//  Copyright © 2016 Rohit Pal. All rights reserved.
//

import UIKit

class SearchOptionCell: UICollectionViewCell {
    
    @IBOutlet weak var buttonImage: UIButton!
}
