//
//  FavouriteDoctorCell.swift
//  Practo Nearby
//
//  Created by Rohit Pal on 24/08/16.
//  Copyright © 2016 Rohit Pal. All rights reserved.
//

import UIKit

class FavouriteDoctorCell: UITableViewCell {

    @IBOutlet weak var removeFavouriteButton: UIButton!
    @IBOutlet weak var doctorPhoto: UIImageView!
    @IBOutlet weak var doctorQualifications: UILabel!
    @IBOutlet weak var doctorName: UILabel!
    @IBOutlet weak var doctorSpecialities: UILabel!
    @IBOutlet weak var doctorLocality: UILabel!
    
    var returnObject : Doctor? = nil
    var returnBlock : ((Doctor) -> Void)? = nil
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func setupCell(doctor : Doctor, index:Int){
        let placeholderImage = UIImage(named: "CarouselPlaceholder")
        var imageUrl:String?
        if doctor.photos?.count > 0 {
            imageUrl = doctor.defaultDoctorPhoto?.url
        } else {
            // Load a placeholder Image
            imageUrl = "https://www.placehold.it/64x64?text=NA"
        }
        self.doctorName.text = doctor.name
        var qualifications:String = ""
        
        for qual in doctor.qualifications!{
            qualifications += "\(qual.qualification!), "
        }
        self.doctorQualifications.text = qualifications
        self.doctorSpecialities.text = doctor.specialities![0].speciality
        self.doctorLocality.text = doctor.locality
        self.doctorPhoto.kf_setImageWithURL(NSURL(string: imageUrl! )!,
                                            placeholderImage: placeholderImage)
        self.removeFavouriteButton.tag = index
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

