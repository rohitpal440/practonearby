//
//  PracticeLocationCell.swift
//  Practo Nearby
//
//  Created by Rohit Pal on 24/08/16.
//  Copyright © 2016 Rohit Pal. All rights reserved.
//

import UIKit
import GoogleMaps

class PracticeLocationCell: UITableViewCell {


    @IBOutlet weak var distanceFromUser: UILabel!
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var practiceLocation: UILabel!
    @IBOutlet weak var practiceName: UILabel!
    var currentLocation = NetworkTask.sharedInstance.currentLocation
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func setupCell(doctor:Doctor){
        self.distanceFromUser.text = "~\(round(doctor.distance!)) kms away"
        self.practiceName.text = doctor.practiceName!
        self.practiceLocation.text = doctor.locality!
        mapView.myLocationEnabled = true
        mapView.settings.scrollGestures = false
        mapView.settings.zoomGestures = true
        let position = CLLocationCoordinate2D(latitude: doctor.latitude!,
                                              longitude: doctor.longitude!)
        let marker = GMSMarker(position: position)
        marker.icon = UIImage(named: "LocationMarkerDefault")
        marker.appearAnimation = kGMSMarkerAnimationPop
        marker.map = mapView
        mapView.camera = GMSCameraPosition.cameraWithTarget(position, zoom: 12)
        self.contentView.bringSubviewToFront(self.distanceFromUser)
    }
    
}
