//
//  NetworkTask.swift
//  Practo Nearby
//
//  Created by Rohit Pal on 19/08/16.
//  Copyright © 2016 Rohit Pal. All rights reserved.
//

import Foundation
import Alamofire
import GoogleMaps

class NetworkTask: NSObject {
    
    static let sharedInstance = NetworkTask()
    
    let cityAlias:[String:String] = [
        "Bangalore Urban" : "Bangalore",
        "New Delhi" : "Delhi"
    ]
    
    var currentLocation:CLLocation?
    static var currentCity:String?
    var locationManager: CLLocationManager!
    var locationReturnBlock : ((location: CLLocation) -> Void)? = nil
    let keysPath:String? = NSBundle.mainBundle().pathForResource("Keys",
                                                            ofType: "plist")!
    var apiKeys:Dictionary<String, String> {
        return  NSDictionary(contentsOfFile: keysPath!) as! Dictionary
    }
    
    func getUserCoordinate(){
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
    }
    
    func getCityName(currentLocation: CLLocation,
                     completionHandler:(cityName:String)->()){
        let lat = currentLocation.coordinate.latitude
        let lng = currentLocation.coordinate.longitude
        let latlng:String! = "\(lat),\(lng)"
        Alamofire.request(.GET,
            "https://maps.googleapis.com/maps/api/geocode/json",
            parameters: ["latlng": latlng,
                "key": apiKeys["GoogleGeoCodingApiKey"]!])
            .responseJSON { response in
                if let JSON = response.result.value {
                    if let results = JSON["results"] as? [[String:AnyObject]] {
                        let result = results[0]
                        let addressComponent = result["address_components"]
                            as? [AnyObject]
                        let city = addressComponent![5]
                        let cityName = city["long_name"]
                        if let cityName = self.cityAlias[cityName as! String] {
                            NetworkTask.currentCity = cityName
                            NSUserDefaults.standardUserDefaults()
                                .setObject(cityName, forKey: "cityName")
                            NSUserDefaults.standardUserDefaults().synchronize()
                            completionHandler(cityName:cityName)
                            }
                    }
                }
        }
    }
    
    func getCurrentLocation(completion:((location : CLLocation)->Void)? = nil){
        self.getUserCoordinate()
        locationReturnBlock = completion
    }
    
    func getNearbyDoctors(city:String, latlng:String,speciality:String,
                          completionHandler: ([Doctor]?,NSError?)->()){
        let headers = [
            "X-CLIENT-ID": apiKeys["PractoClientId"]!,
            "X-API-KEY": apiKeys["PractoApiKey"]!,
            ]
        
        Alamofire.request(.GET, "https://api.practo.com/search/",
            parameters: ["city" : city, "near":latlng,"speciality":speciality],
            headers: headers)
            .responseJSON { response in
                switch response.result {
                case .Success(let value):
                    var doctorsList:[Doctor] = []
                    let doctors = value["doctors"]! as? [AnyObject]
                    for i in 0..<doctors!.count {
                        let jsonDoctor = doctors![i]
                        let doctor = Doctor(json: jsonDoctor)
                        doctorsList.append(doctor)
                    }
                    
                    completionHandler(doctorsList, nil)
                case .Failure(let error):
                    completionHandler(nil, error)
                }
        }
    }
    
    func getPracticeAddress(id:String,
                            completionHandler: (String?,NSError?)->()) {
        let headers = [
            "X-CLIENT-ID": apiKeys["PractoClientId"]!,
            "X-API-KEY": apiKeys["PractoApiKey"]!,
            ]
        Alamofire.request(.GET, "https://api.practo.com/id",
            parameters: [:],headers: headers)
            .responseJSON { response in
                switch response.result {
                case .Success(let value):
                    let streetAddress = value["street_address"] as? String
                    completionHandler(streetAddress, nil)
                case .Failure(let error):
                    completionHandler(nil, error)
                }
        }
    }
    
    func noResultFoundAlert(delegate:UIViewController){
        let AlertView = UIAlertController(title: "No Result Found!",
                    message: "Unable to find the speciality searched by You",
                    preferredStyle: UIAlertControllerStyle.Alert)
        AlertView.addAction(UIAlertAction(title: "OK",
            style: UIAlertActionStyle.Cancel, handler:nil))
        delegate.presentViewController(AlertView, animated: true,
                                       completion: nil)
    }
    
}

extension NetworkTask: CLLocationManagerDelegate {
    func locationManager(manager: CLLocationManager,
                didChangeAuthorizationStatus status: CLAuthorizationStatus) {
        if CLLocationManager.locationServicesEnabled() {
            if status == .AuthorizedWhenInUse || status == .AuthorizedAlways {
                if #available(iOS 9.0, *) {
                    locationManager.requestLocation()
                } else {
                    // todo
                    // Fallback on earlier versions
                }
            } else {
                print("Location is not allowed")
            }
        } else {
            // show setting screen for location change
//            if let settingsURL =
//                NSURL(string: UIApplicationOpenSettingsURLString) {
//                UIApplication.sharedApplication().openURL(settingsURL)
//            }
            print("Location services are not enabled. Please turn on your GPS")
        }
        
    }
    
    func locationManager(manager: CLLocationManager,
                         didUpdateLocations locations: [CLLocation]) {
        if let location = locations.first {
            currentLocation = location
            let latlng:String! =
              "\(location.coordinate.latitude),\(location.coordinate.longitude)"
            NSUserDefaults.standardUserDefaults().setObject(latlng,
                                                            forKey: "latlng")
            manager.stopUpdatingLocation()
            self.locationManager = nil
            self.locationReturnBlock?(location: location)
        }
        
    }
    
    func locationManager(manager: CLLocationManager,
                         didFailWithError error: NSError) {
        print(error.code)
        print(error.description)
    }
}

extension NetworkTask {
    func isConnected(delegate:UIViewController) -> Bool {
        if Reachability.isConnectedToNetwork() == true {
            print("Internet connection: OK")
            return true
        } else {
            print("Internet connection FAILED")
            let AlertView = UIAlertController(
                title: "No Internet Connnection!",
                message: "Make sure your device is connected to the internet",
                preferredStyle: UIAlertControllerStyle.Alert)
            AlertView.addAction(UIAlertAction(title: "OK",
                style: UIAlertActionStyle.Cancel, handler:nil))
            delegate.presentViewController(AlertView, animated: true,
                                           completion: nil)
            return false
        }
    }
    
}