//
//  ViewController.swift
//  Practo Nearby
//
//  Created by Rohit Pal on 17/08/16.
//  Copyright © 2016 Rohit Pal. All rights reserved.
//

import UIKit
import GoogleMaps
class EntryController: UIViewController {

    @IBOutlet weak var progressBar: UIProgressView!
    var timer = NSTimer()
    var currentLocation:CLLocation?
    var currentCity: String?
    var bootWithInternet:Bool = false
    override func viewDidLoad() {
        
        // hide the navigation bar while opening and loading data from server
        self.navigationController?.navigationBarHidden = true
        
//        if Reachability.isConnectedToNetwork() == true {
//            bootAppWithInternet()
//            bootWithInternet = true
//        } else {
//            print("No internet Available")
//            showFavouriteDoctors()
//        }
        super.viewDidLoad()
    }

    func showFavouriteDoctors() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let favouriteDoctorsController =
            storyboard.instantiateViewControllerWithIdentifier(
                "FavouriteDoctors") as! FavouriteDoctors
        self.navigationController?.pushViewController(
            favouriteDoctorsController, animated: true)
    }
    
    func showFavouriteAlert(){
        let AlertView = UIAlertController(
            title: "No Internet Connnection!",
            message: "Make sure your device is connected to the internet" +
                "Still You can See Favourite Doctors",
            preferredStyle: UIAlertControllerStyle.Alert)
        
        // Create the actions
        let showFavouriteAction = UIAlertAction(title: "Show Favourites",
                                     style: UIAlertActionStyle.Default) {
            UIAlertAction in
                self.showFavouriteDoctors()
        }
        
        // Add the actions
        AlertView.addAction(showFavouriteAction)
        
        // Present the controller
        self.presentViewController(AlertView, animated: true,
                                   completion: nil)
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        if Reachability.isConnectedToNetwork() == true {
            if bootWithInternet == false {
                bootAppWithInternet()
                bootWithInternet = true
            } else {
                print("App is already booted with internedt")
            }
        } else {
            showFavouriteAlert()
        }
    }
    
    
    func bootAppWithInternet(){
        // get current coordinates of users
        NetworkTask.sharedInstance
            .getCurrentLocation {[weak self] (location) in
                self?.currentLocation = location
                let lat = location.coordinate.latitude
                let lng = location.coordinate.longitude
                let latlng:String! = "\(lat),\(lng)"
                self!.progressBar.progress += 0.3
                NetworkTask.sharedInstance.getCityName(location){ cityName in
                    self?.currentCity = cityName as String
                    self!.progressBar.progress += 0.3
                    NetworkTask.sharedInstance
                        .getNearbyDoctors(cityName, latlng: latlng,
                        speciality:" ") { doctorList, error in
                            self!.showMap((self?.currentLocation)!,
                                doctorsList: doctorList!)
                            self!.progressBar.progress += 0.3
                    }
                }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    func showMap(currentLocation:CLLocation, doctorsList:[Doctor]) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let mapController = storyboard
            .instantiateViewControllerWithIdentifier("MapController")
            as! MapController
        mapController.doctorsList = doctorsList
        mapController.currentLocation = currentLocation
        navigationController?.pushViewController(mapController, animated: true)
    }
}

