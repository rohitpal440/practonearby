//
//  Doctor.swift
//  Practo Nearby
//
//  Created by Rohit Pal on 21/08/16.
//  Copyright © 2016 Rohit Pal. All rights reserved.
//

import Foundation

class Doctor {
    var name: String?
    var gender: Character?
    var currency: String?
    var locality: String?
    var city: String?
    var longitude: Double?
    var latitude: Double?
    var distance: Double?
    var experienceYears: Int?
    var doctorId: Int?
    var practiceID: Int?
    var practiceName: String?
    var summary:String?
    var recommendationPercent: Int?
    var recommendation: Int?
    var appointmentExperienceScore: Int?
    var patientExpereinceScore: Int?
    var onCall: Bool?
    var consultationFees: Int?
    var consultationFree: Bool?
    var serviceCount: Int?
    var popularServices: [String]?
    var availability:[String: Bool]?
    var qualifications:[Qualification]?
    var specialities:[Speciality]?
    var photos: [Photo]?
    var practicePhotos: [PracticePhoto]?
    var practiceLogo:PracticePhoto?
    var defaultDoctorPhoto: Photo?
    var visitTiming: [String: [String: String]]?
    var doctorJson:AnyObject?
    init(json:AnyObject) {
        self.doctorJson = json
        self.name = json["doctor_name"] as? String
        self.gender = json["gender"] as? Character
        self.currency = json["currency"] as? String
        self.locality = json["locality"] as? String
        self.city = json["city"] as? String
        self.longitude = json["longitude"] as? Double
        self.latitude = json["latitude"] as? Double
        self.distance = Double((json["distance"] as? String)!)
        self.experienceYears = json["experience_years"] as? Int
        self.doctorId = json["doctor_id"] as? Int
        self.practiceID = json["practice_id"] as? Int
        self.practiceName = json["practice_name"] as? String
        self.summary = json["summary"] as? String
        self.recommendationPercent = json["recommendation_percent"] as? Int
        self.recommendation = json["recommendation"] as? Int
        self.appointmentExperienceScore = json["appointment_experience_score"]
            as? Int
        self.patientExpereinceScore = json["patient_experience_score"] as? Int
        self.onCall = json["on_call"] as? Bool
        self.consultationFees = json["consultation_fees"] as? Int
        self.consultationFree = json["consultation_free"] as? Bool
        self.serviceCount = json["service_count"] as? Int
        self.popularServices = json["popular_services"] as? [String]
        let availablityDict = json["availibility"] as? NSDictionary
        self .availability = availablityDict! as? [String : Bool]
        self.qualifications = setQualifications((json["qualifications"]
            as? NSArray)!)
        self.specialities = setSpecialities((json["specialties"]
            as? NSArray)!)
        self.photos = setPhotos((json["photos"] as? NSArray)!)
        self.practicePhotos = setPracticePhotos((json["practice_photos"]
            as? NSArray)!)
        let visitTimingJson = json["visit_timings"] as? NSDictionary
        self.visitTiming = setVisitTiming((visitTimingJson)!)
    }
    
    func setQualifications(jsonArray: NSArray) -> [Qualification] {
        var qualifications: [Qualification] = []
        for value in jsonArray {
            let qual = Qualification(json: (value))
            qualifications.append(qual)
            
        }
        return qualifications
    }
    
    func setSpecialities(jsonArray: NSArray) -> [Speciality] {
        var specialities: [Speciality] = []
        for value in jsonArray {
            let s = Speciality(json: (value))
            specialities.append(s)
            
        }
        return specialities
    }
    
    func setPhotos(jsonArray: NSArray) -> [Photo] {
        var photos: [Photo] = []
        for value in jsonArray {
            let photo = Photo(json: (value))
            if(photo.isDefault == true){
                self.defaultDoctorPhoto = photo
            }
            photos.append(photo)
        }
        return photos
    }
    
    func setPracticePhotos(jsonArray: NSArray) -> [PracticePhoto] {
        var practicePhotos: [PracticePhoto] = []
        for value in jsonArray {
            let practicePhoto = PracticePhoto(json: (value))
            if((practicePhoto.isLogo) == true){
                self.practiceLogo = practicePhoto
            }
            practicePhotos.append(practicePhoto)
            
        }
        return practicePhotos
    }

    func setVisitTiming(json: AnyObject) -> [String: [String: String]] {
        let days = [
            "monday",
            "tuesday",
            "wednesday",
            "thursday",
            "friday",
            "saturday",
            "sunday"
        ]
        var visitTiming:[String: [String:String]] = [:]
        var allSession:[String:String]=[:]
        for day in days {
            if (availability![day] == true) {
                let allSessionData = json[day]
                allSession["sesession1_start_time"] =
                    allSessionData!!["session1_start_time"] as? String
                allSession["sesession2_start_time"] =
                    allSessionData!!["session2_start_time"] as? String
                allSession["sesession1_end_time"] =
                    allSessionData!!["session1_end_time"] as? String
                allSession["sesession2_end_time"] =
                    allSessionData!!["session2_end_time"] as? String
                
                visitTiming[day] = allSession
            }
            
            
        }
        return visitTiming
    }
    
}

// Convert Doctor Data back to JSON for storing it in data base
extension Doctor {
    func toNSString() -> NSString?{
        
        do{
            let data = try NSJSONSerialization.dataWithJSONObject(doctorJson!,
                                    options: NSJSONWritingOptions.PrettyPrinted)
            if let json = NSString(data: data, encoding: NSUTF8StringEncoding) {
                return json
            }
        } catch {
                print("Unable to convert the data to string")
        }
        return nil
    }
}